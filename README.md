# README #

Project to create a publicly-available open-sourced digitized database of ham radio operators for use in GIS and/or logging software without relying on proprietary websites. Your help is welcomed (and greatly encouraged!)

### What is this repository for? ###

* As stated above - This will eventually be a database of ham operators, updated regularly once the main index is digitized.
* Version 0.01

### How do I get set up? ###

* Files in CSV format at the moment, for interopability
* Due to the large size of database (currently over 1.4 million rows), files will be split up to allow for geocoding.

### Contribution guidelines ###

* Just want to geocode the data at the moment. Anyone with a working PostGIS setup who can digitize already is more than welcome to fork and pull this data. I'm currently working on getting it set up on my box in my free time.

### Who do I talk to? ###

* Robert Ross Wardrup, KD5LPB. minorsecond at gmail dot com